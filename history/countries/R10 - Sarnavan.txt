government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = west_harimari
add_accepted_culture = rajnadhid
religion = high_philosophy
technology_group = tech_harimari
religious_school = golden_palace_school
capital = 4510

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Laxanyar"
		dynasty = "of the Lotus Claw"
		birth_date = 1386.1.20
		adm = 3
		dip = 4
		mil = 5
		culture = west_harimari
	}
}