government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = west_harimari
add_accepted_culture = ghavaanaj
religion = high_philosophy
technology_group = tech_harimari
religious_school = golden_palace_school
capital = 4485

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Vijaysen"
		dynasty = "of the Desert Thunder"
		birth_date = 1392.11.14
		adm = 3
		dip = 5
		mil = 2
		culture = west_harimari
	}
}