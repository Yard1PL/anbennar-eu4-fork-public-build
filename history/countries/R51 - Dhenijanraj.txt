government = monarchy
add_government_reform = raja_reform
government_rank = 2
primary_culture = west_harimari
add_accepted_culture = rabhidarubsad
add_accepted_culture = dhukharuved
add_accepted_culture = sarniryabsad
religion = high_philosophy
technology_group = tech_harimari
capital = 4411
religious_school = golden_palace_school
historical_rival = R38

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Indranayar"
		dynasty = "of the Lotus Claw"
		birth_date = 1359.12.7
		adm = 1
		dip = 1
		mil = 1
		culture = west_harimari
	}
	add_ruler_personality = babbling_buffoon_personality
	add_ruler_personality = craven_personality
	add_ruler_personality = naive_personality
}