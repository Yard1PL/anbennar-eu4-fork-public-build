government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = west_harimari
religion = high_philosophy
technology_group = tech_harimari
religious_school = golden_palace_school
capital = 4405

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Jayapalar"
		dynasty = "of the Golden Paw"
		birth_date = 1377.1.30
		adm = 2
		dip = 1
		mil = 1
		culture = west_harimari
	}
}