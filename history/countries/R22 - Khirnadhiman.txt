government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = rasarhid
religion = high_philosophy
technology_group = tech_raheni
religious_school = orange_sash_school
capital = 4499

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Hemantsenar"
		dynasty = "of the Endless Moon"
		birth_date = 1409.8.9
		adm = 1
		dip = 3
		mil = 6
		culture = west_harimari
	}
}