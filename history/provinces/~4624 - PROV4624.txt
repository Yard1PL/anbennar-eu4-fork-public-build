# No previous file for PROV4624
owner = R64
controller = R64
add_core = R64
culture = brown_orc
religion = high_philosophy

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = tropical_wood

capital = ""

is_city = yes

add_permanent_province_modifier = {
	name = human_minority_opressed_large
	duration = -1
}