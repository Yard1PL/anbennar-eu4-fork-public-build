namespace = adventurerspawnables

#Sornic Sons - Bloodgroves
country_event = {
	id = adventurerspawnables.1
	title = adventurerspawnables.1.t
	desc = adventurerspawnables.1.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = bloodgroves_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		is_year = 1500
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.1
			A09 = { exists = no }
		}
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				bloodgroves_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = bloodgroves_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		
		random_province = {
			limit = {
				region = bloodgroves_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
				has_port = yes
			}
			
			save_event_target_as = spawn_province
		}		
		}
	}
	
	option = {
		name = adventurerspawnables.1.a
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H40
		}
		
		hidden_effect = {
			H40 = {
				country_event = {
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H40
		}

		switch_tag = H40
		
		hidden_effect = {
			H40 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
		
	}
	

}

#Onyx Legion - Reaper's Coast
country_event = {
	id = adventurerspawnables.2
	title = adventurerspawnables.2.t
	desc = adventurerspawnables.2.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = reapers_coast_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		is_year = 1500
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				reapers_coast_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = reapers_coast_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		
		random_province = {
			limit = {
				region = reapers_coast_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
				has_port = yes
			}
			
			save_event_target_as = spawn_province
		}
		}

	}
	
	option = {
		name = adventurerspawnables.1.a
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H42
		}
	
		hidden_effect = {
			H42 = {
				country_event = {
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H42
		}

		switch_tag = H42
		
		hidden_effect = {
			H42 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}


#Shining Circle - Ynn west
country_event = {
	id = adventurerspawnables.3
	title = adventurerspawnables.3.t
	desc = adventurerspawnables.3.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H44
		}
		
		hidden_effect = {
			H44 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H44
		}

		switch_tag = H44
		
		hidden_effect = {
			H44 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}



#Istralorian Crusaders - Ynn east
country_event = {
	id = adventurerspawnables.4
	title = adventurerspawnables.4.t
	desc = adventurerspawnables.4.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = veykoda_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
		is_religion_enabled = corinite
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				veykoda_region = { has_discovered = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			A45 = {
				NOT = { religion = corinite }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = veykoda_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H46
		}
		
		hidden_effect = {
			H46 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H46
		}

		switch_tag = H46
		
		hidden_effect = {
			H46 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}

#Menibor Free Company - Ynn west
country_event = {
	id = adventurerspawnables.5
	title = adventurerspawnables.5.t
	desc = adventurerspawnables.5.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {	
		has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
		is_religion_enabled = corinite
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H48
		}
		
		hidden_effect = {
			H48 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H48
		}

		switch_tag = H48
		
		hidden_effect = {
			H48 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}


#Varbuks Freemen - Forest of the Cursed Ones
country_event = {
	id = adventurerspawnables.6
	title = adventurerspawnables.6.t
	desc = adventurerspawnables.6.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = forest_of_the_cursed_ones_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		is_year = 1550
		any_province = {
			OR = {
				continent = north_america
				continent = south_america
			}
			trade_goods = slaves
		}
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = forest_of_the_cursed_ones_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H50
		}
		
		hidden_effect = {
			H50 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Varbuk"
					adm = 4
					dip = 3
					mil = 5
					age = 37
					claim = 95
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H50
		}

		switch_tag = H50
		
		hidden_effect = {
			H50 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Varbuk"
					adm = 4
					dip = 3
					mil = 5
					age = 37
					claim = 95
				}
			}
		}
	}
}


#House of Pelodir - Ynn (they should get events to absorb ynnic countries) west
country_event = {
	id = adventurerspawnables.7
	title = adventurerspawnables.7.t
	desc = adventurerspawnables.7.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H52
		}
		
		hidden_effect = {
			H52 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Pelodir the Great"
					adm = 6
					dip = 3
					mil = 3
					age = 350
					claim = 95
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H52
		}

		switch_tag = H52
		
		hidden_effect = {
			H52 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Pelodir the Great"
					adm = 6
					dip = 3
					mil = 3
					age = 350
					claim = 95
				}
			}
		}
	}
}

#Tipney Pioneers - Ynn west
country_event = {
	id = adventurerspawnables.8
	title = adventurerspawnables.8.t
	desc = adventurerspawnables.8.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		has_global_flag = halfling_revolt 
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H54
		}
		
		hidden_effect = {
			H54 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H54
		}

		switch_tag = H54
		
		hidden_effect = {
			H54 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}


#Havoral Band - Ynn east
country_event = {
	id = adventurerspawnables.9
	title = adventurerspawnables.9.t
	desc = adventurerspawnables.9.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = veykoda_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				veykoda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = veykoda_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H56
		}
		
		hidden_effect = {
			H56 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H56
		}

		switch_tag = H56
		
		hidden_effect = {
			H56 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}


#Chipped Tooth Company - Ynn west
country_event = {
	id = adventurerspawnables.10
	title = adventurerspawnables.10.t
	desc = adventurerspawnables.10.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
		any_province = {
			OR = {
				continent = north_america
				continent = south_america
			}
			trade_goods = slaves
		}
		is_religion_enabled = corinite
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				veykoda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H58
		}
		
		hidden_effect = {
			H58 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H58
		}

		switch_tag = H58
		
		hidden_effect = {
			H58 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}

#Plumwall Expedition - Ynn west
country_event = {
	id = adventurerspawnables.11
	title = adventurerspawnables.11.t
	desc = adventurerspawnables.11.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		#has_global_flag = new_world_discovered
		any_province = {
			OR = {
				region = epednan_expanse_region
				region = sarda_region
			}
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
		is_year = 1510
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				sarda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				OR = {
					region = epednan_expanse_region
					region = sarda_region
				}
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		
		random_province = {
			limit = {
				province_id = 1882
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H60
		}
		
		hidden_effect = {
			H60 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H60
		}

		switch_tag = H60
		
		hidden_effect = {
			H60 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}

#Brigade Magnificent - east effelai
country_event = {
	id = adventurerspawnables.12
	title = adventurerspawnables.12.t
	desc = adventurerspawnables.12.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = east_effelai_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
		is_religion_enabled = corinite
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				east_effelai_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = east_effelai_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		
		random_province = {
			limit = {
				region = east_effelai_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
				has_port = yes
			}
			
			save_event_target_as = spawn_province
		}		
		}

	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H62
		}
		
		hidden_effect = {
			H62 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Isobel"
					dynasty = "síl Isobelin"
					adm = 5
					dip = 6
					mil = 2
					age = 48
					claim = 95
					female = yes
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H62
		}

		switch_tag = H62
		
		hidden_effect = {
			H62 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
				define_ruler = {
					name = "Isobel"
					dynasty = "síl Isobelin"
					adm = 5
					dip = 6
					mil = 2
					age = 48
					claim = 95
				}
			}
		}
	}
}

#Bardswood Band - Ynn east
country_event = {
	id = adventurerspawnables.13
	title = adventurerspawnables.13.t
	desc = adventurerspawnables.13.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_global_flag = new_world_discovered
		any_province = {
			region = veykoda_region
			is_empty = yes
			
			#has_discovered = ROOT
			
			has_empty_adjacent_province = yes
		}
		current_age = age_of_reformation
	}
	
	mean_time_to_happen = {
		months = 500
		
		modifier = {
			factor = 0.5
			any_country = {
				capital_scope = { continent = europe }
				veykoda_region = { has_discovered = ROOT }
			}
		}
	}
	
	immediate = {
		hidden_effect = {
		random_province = {
			limit = {
				region = veykoda_region
				is_empty = yes
				
				has_empty_adjacent_province = yes
				
			}
			
			save_event_target_as = spawn_province
		}
		}
		
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H64
		}
		
		hidden_effect = {
			H64 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		event_target:spawn_province = {
			create_colony = 1000
		}
		
		event_target:spawn_province = {
			cede_province = H64
		}

		switch_tag = H64
		
		hidden_effect = {
			H64 = {
				country_event = { 
					id = anb_miscevents.8
					days = 30
				}
			}
		}
	}
}

#Jaherian Exemplars - east effelai
country_event = {
	id = adventurerspawnables.14
	title = adventurerspawnables.14.t
	desc = adventurerspawnables.14.d
	picture = CARIBBEAN_PIRATE_FORT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owns = 496 #Elizna
		has_global_flag = new_world_discovered
		
		NOT = { exists = H66 }
		NOT = { exists = H67 }
		primary_culture = sun_elf
		religion = bulwari_sun_cult
		is_year = 1500
		
		
		any_province = {
			region = dry_coast_region
			is_empty = yes
			
			has_empty_adjacent_province = yes
		}
	}
	
	mean_time_to_happen = {
		months = 240
		
		modifier = {
			factor = 0.01
			
			is_year = 1520
		}
	}
	
	immediate = {
		hidden_effect = {
			random_province = {
				limit = {
					region = dry_coast_region
					is_empty = yes
					
					has_empty_adjacent_province = yes
					
				}
				
				save_event_target_as = spawn_province
			}
			
			random_province = {
				limit = {
					region = dry_coast_region
					is_empty = yes
					
					has_empty_adjacent_province = yes
					
					has_port = yes
				}
				
				save_event_target_as = spawn_province
			}
			if = {
				limit = {
					2302 = {
						is_empty = yes
						
						has_empty_adjacent_province = yes
					}
				}
				2302 = {
					save_event_target_as = spawn_province
				}
			}
			if = {
				limit = {
					ai = no
					2333 = {
						is_empty = yes
						
						has_empty_adjacent_province = yes
					}
				}
				2333 = {
					save_event_target_as = spawn_province
				}
			}
		}
	}
	
	option = {
		name = adventurerspawnables.1.a 
		
		if = {
			limit = { NOT = { exists = H66 } }
			event_target:spawn_province = {
				create_colony = 1000
			}
			
			event_target:spawn_province = {
				cede_province = H66
			}
			hidden_effect = {
				H66 = {
					country_event = { 
						id = anb_miscevents.8
						days = 30
					}
				}
			}
		}
	}
	
	option = { # play as the pirates
		name = adventurerspawnables.1.b
		trigger = {
			ai = no
		}
		if = {
			limit = { NOT = { exists = H66 } }
			event_target:spawn_province = {
				create_colony = 1000
			}
			
			event_target:spawn_province = {
				cede_province = H66
			}
			hidden_effect = {
				H66 = {
					country_event = { 
						id = anb_miscevents.8
						days = 30
					}
				}
			}
		}
		trigger_switch = {
			on_trigger = total_development
			
			1000 = { H66 = { add_treasury = 2000 add_manpower = 30 } }
			700 = { H66 = { add_treasury = 1600 add_manpower = 23 } }
			600 = { H66 = { add_treasury = 1400 add_manpower = 21 } }
			500 = { H66 = { add_treasury = 1200 add_manpower = 18 } }
			400 = { H66 = { add_treasury = 1000 add_manpower = 15 } }
			350 = { H66 = { add_treasury = 700 add_manpower = 9 } }
			300 = { H66 = { add_treasury = 550 add_manpower = 7 } }
			250 = { H66 = { add_treasury = 450 add_manpower = 5 } }
			200 = { H66 = { add_treasury = 350 add_manpower = 4 } }
			150 = { H66 = { add_treasury = 250 add_manpower = 3 } }
			100 = { H66 = { add_treasury = 150 add_manpower = 2 } }
			50 = { H66 = { add_treasury = 50 add_manpower = 1 } }
		}
		switch_tag = H66
	}
}
